## pyAPI
Is a basic api, provides and lets you manage a basic list of items.

# Testing:
Run the following command to test localy (require docker and docker-compose installed on your localhost).

docker-compose build

docker-compose up


- Get API version:

curl http://localhost:5555/api

- Get API usage:

curl http://localhost:5555/api/usage

- Retrieve the items list:

curl http://localhost:5555/api/lista

- Get a single item:

curl http://localhost:5555/api/lista/id1   <-(get the first item)

- Add a new item:

curl http://localhost:5555/api/lista -d "item=Ascochinga" -X POST -v

- Delete an item:

curl http://localhost:5555/lista/id3 -X DELETE -v
