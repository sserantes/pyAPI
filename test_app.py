""" Functional api test """

from flask import url_for
import pytest
from app import create_app


@pytest.fixture(scope='module')
def client():
    """ Define test scope """
    app = create_app()
    app.testing = True
    ctx = app.test_request_context()
    ctx.push()

    yield app.test_client()

    ctx.pop()


def test_api_root(client):
    """
    Test if the api respond
    """
    response = client.get(url_for('main.api_root'))
    assert b"apiVersion" in response.data
    assert response.status_code == 200

def test_api_lista(client):
    """
    Test if the api_lista respond
    """
    response = client.get(url_for('main.api_lista'))
    assert response.status_code == 200

def test_api_lista_post(client):
    """
    Test if the api_lista POST respond
    """
    response = client.post(url_for('main.api_lista'))
    assert response.status_code == 201

def test_api_item_delete(client):
    """
    Test if the api_item DELETE respond
    """
    response = client.delete(url_for('main.api_lista') + '/id1')
    assert response.status_code == 204
