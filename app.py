""" ONLY FOR DEVELOPMENT PURPOSE DO NOT USE IN PROD """

from flask import Flask
from modules.main import main


def create_app():
    """ Create Flask app """
    app = Flask(__name__)
    app.register_blueprint(main)
    return app


API = create_app()


if __name__ == '__main__':
    API.run(debug=True, host='0.0.0.0', port=5555)
