FROM python:3.7-slim-buster
COPY . /app
RUN pip3 install -r /app/requirements.txt
CMD python3 /app/app.py
