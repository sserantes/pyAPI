from flask import Blueprint, render_template, abort, request
from flask_restful import reqparse

APIVERSION = '1.0'
APIUSAGE = "Retrieve the items list: .../api/lista\nGet a single item: .../api/lista/id1   <-(get the first item)\nAdd a new item: .../api/lista -d \"item=Ascochinga\" -X POST -v\n"


LISTA = {
    'id1': {'item': 'Cerro Azul'},
    'id2': {'item': 'Unquillo'},
    'id3': {'item': 'Dique la Quebrada'},
}

def if_not_exist(id_id):
    """
       Function to abort if item doesn't exist
    """
    if id_id not in LISTA:
        abort(404, "Id does not exist")

PARSER = reqparse.RequestParser()
PARSER.add_argument('item')


main = Blueprint('main', __name__)

"""
@main.route('/', defaults={'page': 'index'})
@main.route('/<page>')
def show(page):
    return ("%s" % page)
"""

@main.route('/api')
def api_root():
    """ Show api version """
    return ("{\"apiVersion\":\"%s\" - ONLY FOR DEVELOPMENT PURPOSE DO NOT USE IN PROD -\nUse api/usage for help}" % APIVERSION)

@main.route('/api/usage')
def api_usage():
    """ Show api usage """
    return ("{Usage:\n\"%s\"}" % APIUSAGE)

@main.route('/api/lista', methods=['GET', 'POST'])
def api_lista():
    if request.method == 'POST':
        """ Add a new Item """
        args = PARSER.parse_args()
        id_id = int(max(LISTA.keys()).lstrip('id')) + 1
        id_id = 'id%i' % id_id
        LISTA[id_id] = {'item': args['item']}
        return LISTA[id_id], 201
    else:
        return LISTA

# @main.route('/api/lista/', defaults={'id_id': 'id1'})
@main.route('/api/lista/<id_id>', methods=['GET', 'DELETE'])
def api_item(id_id):
    if request.method == 'DELETE':
        """ Delete an Item """
        if_not_exist(id_id)
        del LISTA[id_id]
        return '', 204
    else:
        """ Get an Item """
        if_not_exist(id_id)
        return LISTA[id_id]
